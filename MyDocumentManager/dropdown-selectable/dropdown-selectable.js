﻿(function ($) {
    $(function () {

        var selectDropdownItem = function (event) {
            event.preventDefault();

            // Get id and value of the selected item
            var $selectedListItem = $(this).closest('li');
            var selectedItemId = $selectedListItem.find('.dropdown-item-id').attr('value');
            var selectedItemValue = $selectedListItem.find('.dropdown-item-value').text();

            // Update dropdown with the new selected item
            var $dropdownSelectable = $selectedListItem.closest('.dropdown-selectable');
            $dropdownSelectable.find('.dropdown-item-selected-id').val(selectedItemId);
            $dropdownSelectable.find('.dropdown-item-selected').text(selectedItemValue);

            // Update data targets of each selected item's attribute
            $selectedListItem.find('.dropdown-item-attr').filter(function() {
                return $(this).parentsUntil($selectedListItem, '.dropdown-selectable').length === 0;
            }).each(function() {
                var $attribute = $(this);
                var value = $attribute.attr('value');
                var targetsIds = $attribute.data("target");

                // Isso é gambiarra -- https://stackoverflow.com/a/20227176
                var $targets = $('[id$=' + targetsIds.replace('#', '') + ']');
                $targets.each(function() { $(this).attr('placeholder', value) });

                $targets.each(function() { $(this).val(value) });
            });
        };

        $(document).on('click', '.dropdown-selectable .dropdown-menu a', selectDropdownItem);

    });
})(jQuery);