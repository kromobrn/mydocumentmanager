﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager
{
    public class FeedbackUtils
    {
        public static string MsgFailedToConnectToDatabase = "Could not estabilish a database connection.";
        public static string MsgAllFieldsRequired = "All fields are required!";

        public static string MsgRequiredField(string fieldName)
        {
            return fieldName + " is required!";
        }
    }
}