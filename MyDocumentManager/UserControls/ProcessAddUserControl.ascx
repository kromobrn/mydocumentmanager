﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcessAddUserControl.ascx.cs" Inherits="MyDocumentManager.UserControls.ProcessAddUserControl" %>

<asp:Panel ID="Panel1" runat="server" Height="64px" Width="288px">
    <asp:TextBox ID="txtName" runat="server" Width="280px"></asp:TextBox>
    <br />
    <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-large btn-default">
        Add <span aria-hidden="true" class="glyphicon glyphicon-plus add-icon"></span>
    </asp:LinkButton>
</asp:Panel>
