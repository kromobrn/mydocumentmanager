﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyDocumentManager.Business;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Business.Exceptions;


namespace MyDocumentManager.UserControls
{
    public partial class DocAddUserControl : System.Web.UI.UserControl
    {
        List<Category> categories;
        List<Process> processes;

        public string feedbackMessage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (LoadFields())
            {
                BindFieldsToControls();

                feedbackMessage = FeedbackUtils.MsgAllFieldsRequired;
            }
            else
            {
                feedbackMessage = FeedbackUtils.MsgFailedToConnectToDatabase;
            }

            lblFeedbackMsg.Text = feedbackMessage;
        }

        private bool LoadFields()
        {
            categories = new List<Category>();
            processes = new List<Process>();

            try
            {
                categories = DependencyResolver.Instance.GetNewCategoryServiceInstance().List();
                processes = DependencyResolver.Instance.GetNewProcessServiceInstance().List();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        private void BindFieldsToControls()
        {
            rptCategoriesDropdown.DataSource = categories;
            rptProcessesDropdown.DataSource = processes;

            rptCategoriesDropdown.DataBind();
            rptProcessesDropdown.DataBind();
        }

        private bool ValidateFields()
        {
            
            // E agora como que vou acessar os dropdown que nao sao controles asp

            if (String.IsNullOrEmpty(txtDisplayCode.Text))
            {
                feedbackMessage = "Display code is required";
                return false;
            }

            if (!Int32.TryParse(txtDisplayCode.Text, out int displayCode))
            {
                feedbackMessage = "Display code must be numerical";
                return false;
            }

            if (String.IsNullOrEmpty(txtTitle.Text))
            {
                feedbackMessage = "Title is required";
                return false;
            }

            if (String.IsNullOrEmpty(txtFile.Text))
            {
                feedbackMessage = "File is required";
                return false;
            }

            if (!new Regex(@"(?i)[\w\-. ]+(\.pdf|\.doc|\.xls)").IsMatch(txtFile.Text))
            {
                feedbackMessage = "Only .pdf, .doc and .xls files are allowed";
                return false;
            }

            return true;
        }

        private void ResetFields()
        {
            txtTitle.Text = "";
            txtDisplayCode.Text = "";
            txtFile.Text = "";
            txtCategoryName.Text = "Choose a category";
            // E agora como que vou acessar os dropdown que nao sao controles asp
        }

        private bool GenerateDocumentFromFields(out Document document)
        {
            document = new Business.Entity.Document()
            {
                Title = txtTitle.Text,
                DisplayCode = txtDisplayCode.Text,
                File = txtFile.Text
            };

            string categoryIdField = String.Format("{0}", Request.Form["selectedCategoryId"]);
            string processIdField = String.Format("{0}", Request.Form["selectedProcessId"]);

            if (Int32.TryParse(categoryIdField, out int categoryId))
            {
                document.Category = categories.Where(c => c.Id == categoryId).FirstOrDefault();
                if (document.Category == null)
                {
                    feedbackMessage = "Category is required";
                    return false;
                }
            }
            else
            {
                feedbackMessage = "Category is required";
                return false;
            }

            if (Int32.TryParse(processIdField, out int processId))
            {
                document.Process = processes.Where(p => p.Id == processId).FirstOrDefault();
                if (document.Process == null)
                {
                    feedbackMessage = "Process is required";
                    return false;
                }
            }
            else
            {
                feedbackMessage = "Process is required";
                return false;
            }

            return true;
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields())
                {
                    if (GenerateDocumentFromFields(out Document document))
                    { 
                        DependencyResolver.Instance.GetNewDocumentServiceInstance().Store(document);

                        feedbackMessage = "Document stored successfully";
                        ResetFields();
                    }
                }
            }
            catch (BusinessRuleException bex)
            {
                feedbackMessage = bex.Message.ToString();
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                feedbackMessage = "An unhandled error ocurred. Contact the administrator";
            }

            lblFeedbackMsg.Text = feedbackMessage;
        }
    }
}