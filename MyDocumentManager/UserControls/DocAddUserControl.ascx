﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocAddUserControl.ascx.cs" Inherits="MyDocumentManager.UserControls.DocAddUserControl" %>

<script src="../Scripts/DocAddUserControl.js" type="text/javascript"></script>
<!--<script src="../dropdown-selectable/dropdown-selectable.js" type="text/javascript"></script>-->

<asp:Panel ID="outerPanel" runat="server">
    <asp:Panel ID="panel" runat="server">
        <div class="panel-heading">
            <span class="h2">Document storing</span>
        </div>
        <div class="panel-body container-fluid">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <form ID="AddDocumentForm">
                        <div class="row form-group">
            
                            <div class="col-sm-7">
                                <div class="input-group input-group-lg">

                                    <div class="input-group-btn dropdown-selectable">

                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="dropdown-item-selected">--</span><span class="caret"></span>
                                        </button>

                                        <asp:Repeater runat="server" ID="rptCategoriesDropdown">
                                            <HeaderTemplate>
                                                <ul class="dropdown-menu scrollable-dropdown-menu" role="menu">
                                            </HeaderTemplate>

                                            <ItemTemplate>
                                                <li class="input-lg">
                                                    <a href="#">
                                                        <span class="dropdown-item-value"><%# ((MyDocumentManager.Business.Entity.Category)Container.DataItem).Acronym %></span>
                                                        <span class="small"><%# ((MyDocumentManager.Business.Entity.Category)Container.DataItem).Name %></span>
                                                    </a>
                                                    <input type="hidden" class="dropdown-item-id" value="<%# ((MyDocumentManager.Business.Entity.Category)Container.DataItem).Id %>">
                                                    <input type="hidden" class="dropdown-item-attr" data-target="#txtCategoryName" value="<%# ((MyDocumentManager.Business.Entity.Category)Container.DataItem).Name %>">

                                                </li>
                                            </ItemTemplate>

                                            <FooterTemplate>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <a href="#new">
                                                        New <span aria-hidden="true" class="glyphicon glyphicon-plus-sign add-icon"></span>
                                                    </a>
                                                </li>
                                                </ul>
                                                <input type="hidden" name="selectedCategoryId" class="dropdown-item-selected-id" value="-1">
                                            </FooterTemplate>
                                        </asp:Repeater>

                                    </div>
                            
                                    <asp:TextBox runat="server" ID="txtCategoryName" CssClass="form-control" Placeholder="Choose a category" Disabled="disabled"></asp:TextBox>
                        
                                </div>
                            </div>
            
                            <div class="col-sm-5">
                                <div class="">

                                    <div class="dropdown-selectable">

                                        <button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <span class="dropdown-item-selected">Process</span><span class="caret"></span>
                                        </button>

                                        <asp:Repeater runat="server" ID="rptProcessesDropdown">
                                            <HeaderTemplate>
                                                <ul class="dropdown-menu scrollable-dropdown-menu" role="menu">
                                            </HeaderTemplate>

                                            <ItemTemplate>
                                                <li class="input-lg">
                                                    <a href="#">
                                                        <span class="dropdown-item-value"><%# ((MyDocumentManager.Business.Entity.Process)Container.DataItem).Name %></span>
                                                    </a>
                                                    <input type="hidden" class="dropdown-item-id" value="<%# ((MyDocumentManager.Business.Entity.Process)Container.DataItem).Id %>">
                                                </li>
                                            </ItemTemplate>

                                            <FooterTemplate>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <a href="#new">New <span aria-hidden="true" class="glyphicon glyphicon-plus-sign add-icon"></span>

                                                    </a>
                                                </li>
                                                </ul>
                                                <input type="hidden" name="selectedProcessId" class="dropdown-item-selected-id" value="-1">
                                            </FooterTemplate>
                                        </asp:Repeater>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtDisplayCode" CssClass="form-control input-lg" Placeholder="Display code"></asp:TextBox>
                            </div>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control input-lg" Placeholder="Title"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-12">
                                <asp:TextBox runat="server" ID="txtFile" CssClass="form-control input-lg" Placeholder="File"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="well">
                                <asp:Label ID="lblFeedbackMsg" runat="server" CssClass="input-lg" Text="<%# feedbackMessage %>"></asp:Label>
                            </div>
                        </div>

                        <div class="row form-group">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-large btn-default" OnClick="BtnAdd_Click">
                                Add <span aria-hidden="true" class="glyphicon glyphicon-plus add-icon"></span>
                            </asp:LinkButton>
                        </div>
                    </form>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</asp:Panel>
