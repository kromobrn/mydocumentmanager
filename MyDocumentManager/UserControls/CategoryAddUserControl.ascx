﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryAddUserControl.ascx.cs" Inherits="MyDocumentManager.UserControls.CategoryAddUserControl1" %>

<asp:Panel ID="Panel1" runat="server" Height="64px" Width="288px">
    <asp:TextBox ID="txtAcronym" runat="server" Width="84px"></asp:TextBox>
    <asp:TextBox ID="txtName" runat="server" Width="188px"></asp:TextBox>
    <br />
    <asp:Label ID="lblFeedbackMsg" runat="server" Text="* Required fields"></asp:Label>
    <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-large btn-default">
        Add <span aria-hidden="true" class="glyphicon glyphicon-plus add-icon"></span>
    </asp:LinkButton>
</asp:Panel>