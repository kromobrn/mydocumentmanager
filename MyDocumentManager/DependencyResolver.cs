﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business;
using MyDocumentManager.Model;

namespace MyDocumentManager
{
    // Gambiarra temporária até que eu consiga implementar um container injetor de dependência
    public class DependencyResolver
    {
        private static DependencyResolver instance;

        public static DependencyResolver Instance
        {
            get { return instance ?? (instance = new DependencyResolver()); }
        }

        public DocumentService GetNewDocumentServiceInstance()
        {
            var dbContext = new Model.DBEntities();
            var unityOfWork = new Model.Infrastructure.UnitOfWork(dbContext);
            var docBusiness = new DocumentBusiness(unityOfWork);
            return new DocumentService(docBusiness, unityOfWork);
        }

        public CategoryService GetNewCategoryServiceInstance()
        {
            var dbContext = new Model.DBEntities();
            var unityOfWork = new Model.Infrastructure.UnitOfWork(dbContext);
            var categoryBusiness = new CategoryBusiness(unityOfWork);
            return new CategoryService(categoryBusiness, unityOfWork);
        }

        public ProcessService GetNewProcessServiceInstance()
        {
            var dbContext = new Model.DBEntities();
            var unityOfWork = new Model.Infrastructure.UnitOfWork(dbContext);
            var processBusiness = new ProcessBusiness(unityOfWork);
            return new ProcessService(processBusiness, unityOfWork);
        }
    }
}