﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Business.Exceptions;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class DocumentBusiness : IDocumentBusiness
    {
        private readonly IUnitOfWork unitOfWork;

        public DocumentBusiness(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public void EnsureCanInsert(Document document)
        {
            EnsureDisplayCodeIsUnique(document);
            EnsureCategoryIsNotEmpty(document);
            EnsureProcessIsNotEmpty(document);
        }

        public void EnsureTitleIsNotEmpty(Document document)
        {
            if (String.IsNullOrEmpty(document.File))
            {
                throw new InvalidParameterException("Empty title");
            }
        }

        public void EnsureDisplayCodeIsNotEmpty(Document document)
        {
            if (String.IsNullOrEmpty(document.DisplayCode))
            {
                throw new InvalidParameterException("Empty display code");
            }
        }

        public void EnsureDisplayCodeIsNumerical(Document document)
        {
            if (!Int32.TryParse(document.DisplayCode, out Int32 displayCode))
            {
                throw new InvalidParameterException("Non numerical display code");
            }
        }

        public void EnsureDisplayCodeIsUnique(Document document)
        {
            if (Int32.TryParse(document.DisplayCode, out Int32 displayCode))
            {
                if (unitOfWork.DocumentRepository.Get(d => d.displayCode == displayCode && d.id != document.Id).Count > 0)
                {
                    throw new InvalidParameterException("DisplayCode already been used");
                }
            }
        }
        
        public void EnsureCategoryIsNotEmpty(Document document)
        {
            if (document.Category?.Id == null)
            {
                throw new InvalidParameterException("Category nonexistent");
            }
        }

        public void EnsureProcessIsNotEmpty(Document document)
        {
            if (document.Process?.Id == null)
            {
                throw new InvalidParameterException("Process nonexistent");
            }
        }

        public void EnsureFilePathIsNotEmpty(Document document)
        {
            if (String.IsNullOrEmpty(document.File))
            {
                throw new InvalidParameterException("Empty file");
            }
        }

        public void EnsureFileHasAllowedExtension(Document document)
        {
            string file = document.File;

            string[] allowedExtensions = { ".pdf", ".xls", ".doc" };

            string extension = System.IO.Path.GetExtension(file);

            if (!allowedExtensions.Contains(extension))
            {
                throw new InvalidParameterException("File extension not allowed: \"" + extension + "\"");
            }
        }
    }
}