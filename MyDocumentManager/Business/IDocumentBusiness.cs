﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDocumentManager.Business.Entity;

namespace MyDocumentManager.Business
{
    public interface IDocumentBusiness : IEntityBusiness<Document>
    {
        void EnsureTitleIsNotEmpty(Document document);
        void EnsureDisplayCodeIsNotEmpty(Document document);
        void EnsureDisplayCodeIsNumerical(Document document);
        void EnsureDisplayCodeIsUnique(Document document);
        void EnsureCategoryIsNotEmpty(Document document);
        void EnsureProcessIsNotEmpty(Document document);
        void EnsureFilePathIsNotEmpty(Document document);
        void EnsureFileHasAllowedExtension(Document document);
    }
}
