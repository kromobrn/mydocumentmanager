﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryBusiness categoryBusiness;
        private readonly IUnitOfWork unityOfWork;

        public CategoryService(ICategoryBusiness categoryBusiness, IUnitOfWork unityOfWork)
        {
            this.categoryBusiness = categoryBusiness;
            this.unityOfWork = unityOfWork;
        }

        public List<Category> List()
        {
            var categories = unityOfWork.CategoryRepository.Get().Select(c =>
                new Category(c.id)
                {
                    Acronym = c.acronym,
                    Name = c.name
                });

            return categories.ToList();
        }
    }
}