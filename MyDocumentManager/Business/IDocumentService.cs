﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDocumentManager.Business.Entity;

namespace MyDocumentManager.Business
{
    public interface IDocumentService
    {
        List<Document> Retrieve();
        void Store(Document document);
        //void UpdateStored(Document document);
        //void DeleteStored(Document document);
    }
}
