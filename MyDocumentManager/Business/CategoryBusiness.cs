﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Business.Exceptions;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class CategoryBusiness : ICategoryBusiness
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryBusiness(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public void EnsureCanInsert(Category category)
        {
            EnsureAcronymIsUnique(category);
        }

        public void EnsureAcronymIsUnique(Category category)
        {
            string acronym = category.Acronym;

            if (unitOfWork.CategoryRepository.Get(c => c.acronym == acronym && c.id != category.Id).Count > 0)
            {
                throw new InvalidParameterException("Acronym already been used");
            }
        }
    }
}