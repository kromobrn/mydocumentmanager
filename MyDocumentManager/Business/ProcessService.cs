﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class ProcessService : IProcessService
    {
        private readonly IProcessBusiness processBusiness;
        private readonly IUnitOfWork unityOfWork;

        public ProcessService(IProcessBusiness processBusiness, IUnitOfWork unityOfWork)
        {
            this.processBusiness = processBusiness;
            this.unityOfWork = unityOfWork;
        }

        public List<Process> List()
        {
            var processes = unityOfWork.ProcessRepository.Get().Select(p =>
                new Process(p.id)
                {
                    Name = p.name
                });

            return processes.ToList();
        }
    }
}