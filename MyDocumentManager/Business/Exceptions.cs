﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Exceptions
{
    public class BusinessRuleException : Exception
    {
        public BusinessRuleException(string message) : base(message) { }
    }

    public class InvalidParameterException : BusinessRuleException
    {
        public InvalidParameterException(string message) : base(message) { }
    }

    public class InvalidStateException : BusinessRuleException
    {
        public InvalidStateException(string message) : base(message) { }
    }

    public class InvalidOperationException : BusinessRuleException
    {
        public InvalidOperationException(string message) : base(message) { }
    }
}