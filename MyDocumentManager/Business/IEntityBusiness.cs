﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDocumentManager.Business
{
    public interface IEntityBusiness<Entity> where Entity : Business.Entity.Entity
    {
        void EnsureCanInsert(Entity entity);
        //void EnsureCanUpdate(Entity entity);
        //void EnsureCanDelete(Entity entity);
    }
}
