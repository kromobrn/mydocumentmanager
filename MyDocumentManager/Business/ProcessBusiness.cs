﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Business.Exceptions;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class ProcessBusiness : IProcessBusiness
    {
        private readonly IUnitOfWork unitOfWork;

        public ProcessBusiness(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public void EnsureCanInsert(Process process)
        {
        }
    }
}