﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Business.Entity;
using MyDocumentManager.Model.Infrastructure;

namespace MyDocumentManager.Business
{
    public class DocumentService : IDocumentService
    {
        private readonly IDocumentBusiness documentBusiness;
        private readonly IUnitOfWork unityOfWork;

        public DocumentService(IDocumentBusiness documentBusiness, IUnitOfWork unityOfWork)
        {
            this.documentBusiness = documentBusiness;
            this.unityOfWork = unityOfWork;
        }

        public List<Document> Retrieve()
        {
            var documents = unityOfWork.DocumentRepository.Get().Select(d =>
                new Document(d.id)
                {
                    Title = d.title,
                    DisplayCode = d.displayCode.ToString(),
                    Category = new Category(d.categoryId)
                    {
                        Acronym = d.category.acronym,
                        Name = d.category.name
                    },
                    Process = new Process(d.processId)
                    {
                        Name = d.process.name
                    },
                    File = d.files.FirstOrDefault().path
                });

            return documents.OrderBy(d => d.Title).ToList();
        }

        public void Store(Document document)
        {
            Category category = document.Category;
            Process process = document.Process;
            string file = document.File;

            unityOfWork.DocumentRepository.Add(
                new Model.Document()
                {
                    title = document.Title,
                    displayCode = Int32.Parse(document.DisplayCode),
                    categoryId = document.Category.Id,
                    processId = document.Process.Id,
                    files = new Model.File[] 
                    {
                        new Model.File
                        {
                             title = System.IO.Path.GetFileNameWithoutExtension(file),
                             path = file
                        }
                    }
                });

            documentBusiness.EnsureCanInsert(document);

            unityOfWork.Commit();
        }
    }
}