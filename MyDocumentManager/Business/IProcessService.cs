﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDocumentManager.Business.Entity;

namespace MyDocumentManager.Business
{
    public interface IProcessService
    {
        List<Process> List();
    }
}
