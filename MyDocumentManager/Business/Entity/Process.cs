﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Entity
{
    public class Process : Entity
    {
        public string Name { get; set; }

        public List<Document> AssociatedDocuments { get; set; }

        public Process(int id) : base(id)
        {
        }

        public Process()
        {
        }
    }
}