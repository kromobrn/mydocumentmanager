﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Entity
{
    public class Category : Entity
    {
        public string Acronym { get; set; }
        public string Name { get; set; }

        public List<Document> AssociatedDocuments { get; set; }

        public Category(int id) : base(id)
        {
        }

        public Category()
        {
        }
    }
}