﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Entity
{
    public class Document : Entity
    {
        public string Title { get; set; }
        public string DisplayCode { get; set; }
        public string File { get; set; }
        public Category Category { get; set; }
        public Process Process { get; set; }

        public Document(int id) : base(id)
        {
        }

        public Document()
        {
        }
    }
}