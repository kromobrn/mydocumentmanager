﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Entity
{
    public class File : Entity
    {
        public string Title { get; set; }
        public string Path { get; set; }

        public Document Document { get; set; }

        public File(int id) : base(id)
        {
        }

        public File()
        {
        }
    }
}