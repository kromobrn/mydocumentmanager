﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocumentManager.Business.Entity
{
    public abstract class Entity 
    {
        public int Id { get; }

        public Entity()
        {
        }

        public Entity (int Id)
        {
            this.Id = Id;
        }
    }
}