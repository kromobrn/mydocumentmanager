﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly DBEntities dbContext;

        public CategoryRepository(DBEntities dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<Category> Get(Expression<Func<Category, bool>> predicate = null)
        {
            return this.dbContext.categories.Where(predicate ?? (d => true)).ToList();
        }

        public bool Add(Category data)
        {
            this.dbContext.categories.Add(data);
            return true;
        }

        public bool Delete(Category data)
        {
            this.dbContext.categories.Remove(data);
            return true;
        }

        public bool Update(Category data)
        {
            dbContext.Entry(data).State = EntityState.Modified;
            return true;
        }
    }
}
