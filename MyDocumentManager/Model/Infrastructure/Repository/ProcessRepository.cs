﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public class ProcessRepository : IRepository<Process>
    {
        private readonly DBEntities dbContext;

        public ProcessRepository(DBEntities dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<Process> Get(Expression<Func<Process, bool>> predicate = null)
        {
            return this.dbContext.processes.Where(predicate ?? (d => true)).ToList();
        }

        public bool Add(Process data)
        {
            this.dbContext.processes.Add(data);
            return true;
        }

        public bool Delete(Process data)
        {
            this.dbContext.processes.Remove(data);
            return true;
        }

        public bool Update(Process data)
        {
            dbContext.Entry(data).State = EntityState.Modified;
            return true;
        }
    }
}