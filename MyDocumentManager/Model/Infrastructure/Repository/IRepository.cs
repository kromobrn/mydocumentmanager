﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public interface IRepository<Data> // where Data : ModelObject          (EF buga se editar as classes geradas; nao pude forcar heranca)
    {
        List<Data> Get(Expression<Func<Data, bool>> predicate = null);
        bool Add(Data data);
        bool Update(Data data);
        bool Delete(Data data);
    }
}
