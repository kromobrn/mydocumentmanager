﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public class DocumentRepository : IRepository<Document>
    {
        private readonly DBEntities dbContext;

        public DocumentRepository(DBEntities dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<Document> Get(Expression<Func<Document, bool>> predicate = null)
        {
            return this.dbContext.documents.Where(predicate ?? (d => true)).ToList();
        }

        public bool Add(Document data)
        {
            this.dbContext.documents.Add(data);
            return true;
        }

        public bool Delete(Document data)
        {
            this.dbContext.documents.Remove(data);
            return true;
        }

        public bool Update(Document data)
        {
            dbContext.Entry(data).State = EntityState.Modified;
            return true;
        }
    }
}