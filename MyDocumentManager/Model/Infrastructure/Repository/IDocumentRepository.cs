﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public interface IDocumentRepository : IRepository<Document>
    {
    }
}
