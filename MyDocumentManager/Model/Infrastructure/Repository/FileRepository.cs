﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;

namespace MyDocumentManager.Model.Infrastructure.Repository
{
    public class FileRepository : IRepository<File>
    {
        private readonly DBEntities dbContext;

        public FileRepository(DBEntities dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<File> Get(Expression<Func<File, bool>> predicate = null)
        {
            return this.dbContext.files.Where(predicate ?? (d => true)).ToList();
        }

        public bool Add(File data)
        {
            this.dbContext.files.Add(data);
            return true;
        }

        public bool Delete(File data)
        {
            this.dbContext.files.Remove(data);
            return true;
        }

        public bool Update(File data)
        {
            dbContext.Entry(data).State = EntityState.Modified;
            return true;
        }
    }
}