﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocumentManager.Model.Infrastructure.Repository;

namespace MyDocumentManager.Model.Infrastructure
{
    /// <summary>
    /// Global Unit of Work. 
    /// 
    /// Allows grouping a set of data operations under a single DB context, which can be discarded until final commit.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DBEntities dbContext;

        #region Repositories

        /* FIXME: allow multiple and generic repository support. */

        private DocumentRepository documentRepository;
        private CategoryRepository categoryRepository;
        private ProcessRepository processRepository;
        private FileRepository fileRepository; 

        public DocumentRepository DocumentRepository
        {
            get { return documentRepository ?? (documentRepository = new DocumentRepository(this.dbContext)); }
        }
        public CategoryRepository CategoryRepository
        {
            get { return categoryRepository ?? (categoryRepository = new CategoryRepository(this.dbContext)); }
        }
        public ProcessRepository ProcessRepository
        {
            get { return processRepository ?? (processRepository = new ProcessRepository(this.dbContext)); }
        }
        public FileRepository FileRepository
        {
            get { return fileRepository ?? (fileRepository = new FileRepository(this.dbContext)); }
        }
        #endregion

        public UnitOfWork(DBEntities dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Commit()
        {
            dbContext.SaveChanges();
        }
    }

}