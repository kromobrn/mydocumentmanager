﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDocumentManager.Model.Infrastructure.Repository;

namespace MyDocumentManager.Model.Infrastructure
{
    public interface IUnitOfWork
    {
        DocumentRepository DocumentRepository { get; }
        CategoryRepository CategoryRepository { get; }
        ProcessRepository ProcessRepository { get; }
        FileRepository FileRepository { get; }

        void Commit();
    }
}
