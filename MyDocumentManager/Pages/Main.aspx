﻿<%@ Page Title="Main" Language="C#" MasterPageFile="~/Pages/Site.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="MyDocumentManager.Main" %>

<%@ Register Src="~/UserControls/DocAddUserControl.ascx" TagPrefix="Registration" TagName="DocAdd" %>
<%@ Register Src="~/UserControls/ProcessAddUserControl.ascx" TagPrefix="Registration" TagName="ProcessAdd" %>
<%@ Register Src="~/UserControls/CategoryAddUserControl.ascx" TagPrefix="Registration" TagName="CategoryAdd" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="container" id="documentTableView">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">                    
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Stored documents</h2>
                        </div>
                        <div class="col-md-6">
                            <span class="pull-right align-bottom">
                                <asp:Label ID="lblDocumentsCount" runat="server"><%# storedDocuments.Count %></asp:Label>
                                &nbsp;
                                <small>documents found</small>
                            </span>           
                        </div>
                    </div>
                    
                    <div class="row">
                        <table id="tbDocuments" class="table table-striped table-hover">
                            <asp:Repeater runat="server" ID="rptDocuments">
                                <HeaderTemplate>
                                    <div id="tbDocumentsHeader">
                                        <thead>
                                            <tr>
                                                <th>Process</th>
                                                <th>Category</th>
                                                <th>Display Code</th>
                                                <th>Title</th>
                                                <th>File</th>
                                            </tr>
                                        </thead>
                                    </div>
                                    <tbody>
                                </HeaderTemplate>
                                
                                <ItemTemplate>
                                    <tr class="tbDocumentsRow">
                                        <td><%# ((MyDocumentManager.Business.Entity.Document)Container.DataItem).Process.Name %></td>
                                        <td><%# ((MyDocumentManager.Business.Entity.Document)Container.DataItem).Category.Name %></td>
                                        <td><%# ((MyDocumentManager.Business.Entity.Document)Container.DataItem).DisplayCode%></td>
                                        <td><%# ((MyDocumentManager.Business.Entity.Document)Container.DataItem).Title %></td>
                                        <td><%# ((MyDocumentManager.Business.Entity.Document)Container.DataItem).File %></td>
                                    </tr>
                                </ItemTemplate>

                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-md-offset-2 col-md-2">
                            <button type="button" class="btn btn-large btn-default" data-target="#modalAddDocument" data-toggle="modal">
                               Add <span class="glyphicon glyphicon-plus add-icon"></span> 
                            </button>        
                        </div>
                        <div class="col-md-6">
                            <div class="searchBar form-group has-feedback">
                                <input type="text" class="form-control" placeholder="Search"/>
                                <span class="glyphicon glyphicon-search form-control-feedback search-icon"></span>
                            </div>
                        </div>
                        <div class="col-md-2">

                        </div>
                    </div>
                    
                    <div class="modal fade" id="modalAddDocument" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <Registration:DocAdd runat="server" ID="DocAddUserControl" />
                            </div>
                        </div>
                    </div>
             
            </div>
        </div>
    </div>
    
</asp:Content>
