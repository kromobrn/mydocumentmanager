﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyDocumentManager.Business;
using MyDocumentManager.Business.Entity;

namespace MyDocumentManager
{
    public partial class Main : Page
    {
        public List<Document> storedDocuments;
        public string feedbackMessage;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (RetrieveDocuments())
                {
                    PopulateUI();
                }
                else
                {
                    ShowError();
                }
            }
        }

        private bool RetrieveDocuments()
        {
            try
            {
                storedDocuments = DependencyResolver.Instance.GetNewDocumentServiceInstance().Retrieve();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                feedbackMessage = FeedbackUtils.MsgFailedToConnectToDatabase;

                return false;
            }
                
            return true;
        }

        private void PopulateUI()
        {
            rptDocuments.DataSource = storedDocuments;
            rptDocuments.DataBind();

            lblDocumentsCount.Text = storedDocuments.Count.ToString();
        }

        private void ShowError()
        {
            
        }
    }
}